import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
// import Materials from 'vue-materials'  Vue.use(Materials) Required jQuery -> not working

import App from './App.vue'
import instructors from './profiles/instructors.vue'
import projects from './projects/projects.vue'
import projectDetail from './projects/projectDetail.vue'
import projectCreate from './projects/projectCreate.vue'
import projectEdit from './projects/projectEdit.vue'
import home from './nav/home.vue'

Vue.use(VueResource);
Vue.use(VueRouter);

global.REST_API_HOST = "http://localhost:3001/"

const routes = [
  { path: '/', component: home },
  { path: '/!', component: home },
  { path: '/bar', component: instructors },
  { path: '/projects', component: projects },
  { path: '/projects/create', component: projectCreate },
  { path: '/projects/:id', component: projectDetail, props: true },
  { path: '/projects/:id/edit', component: projectEdit, props: true }
]

const router = new VueRouter({
  routes // short for routes: routes
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
